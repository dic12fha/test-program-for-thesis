////
//// Created by Mikael Bäckman on 2016-12-16.
/* We want POSIX.1-2008 + XSI, i.e. SuSv4, features */
#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <unistd.h>
#include <ftw.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "read_proc.h"
extern char **environ;


/* POSIX.1 says each process has at least 20 file descriptors.
 * Three of those belong to the standard streams.
 * Here, we use a conservative estimate of 15 available;
 * assuming we use at most two for other uses in this program,
 * we should never run into any problems.
 * Most trees are shallower than that, so it is efficient.
 * Deeper trees are traversed fine, just a bit slower.
 * (Linux allows typically hundreds to thousands of open files,
 *  so you'll probably never see any issues even if you used
 *  a much higher value, say a couple of hundred, but
 *  15 is a safe, reasonable value.)
*/
#ifndef USE_FDS
#define USE_FDS 15
#endif

int print_entry(const char *filepath, const struct stat *info,
                const int typeflag, struct FTW *pathinfo)
{

    if (typeflag == FTW_SL) {
        char   *target;
        size_t  maxlen = 1023;
        ssize_t len;

        while (1) {

            target = malloc(maxlen + 1);
            if (target == NULL)
                return ENOMEM;

            len = readlink(filepath, target, maxlen);
            if (len == (ssize_t)-1) {
                const int saved_errno = errno;
                free(target);
                return saved_errno;
            }
            if (len >= (ssize_t)maxlen) {
                free(target);
                maxlen += 1024;
                continue;
            }

            target[len] = '\0';
            break;
        }

        printf(" %s -> %s\n", filepath, target);
        free(target);

    } else
    if (typeflag == FTW_SLN)
        printf(" %s (dangling symlink)\n", filepath);
    else
    if (typeflag == FTW_F || typeflag == FTW_D || typeflag == FTW_DP) {
        struct stat fileStat;
        stat(filepath, &fileStat);
        printf((S_ISDIR(fileStat.st_mode)) ? "d" : "-");
        printf((fileStat.st_mode & S_IRUSR) ? "r" : "-");
        printf((fileStat.st_mode & S_IWUSR) ? "w" : "-");
        printf((fileStat.st_mode & S_IXUSR) ? "x" : "-");
        printf((fileStat.st_mode & S_IRGRP) ? "r" : "-");
        printf((fileStat.st_mode & S_IWGRP) ? "w" : "-");
        printf((fileStat.st_mode & S_IXGRP) ? "x" : "-");
        printf((fileStat.st_mode & S_IROTH) ? "r" : "-");
        printf((fileStat.st_mode & S_IWOTH) ? "w" : "-");
        printf((fileStat.st_mode & S_IXOTH) ? "x" : "-");
        printf(" %3.0ld %5ld %5ld   ", fileStat.st_nlink, fileStat.st_uid, fileStat.st_gid);
        
    }
    else
    if (typeflag == FTW_DNR)
        printf(" %s/ (unreadable)\n", filepath);
    else
        printf(" %s (unknown)\n", filepath);

    // get number of links for the file 


    /* const char *const filename = filepath + pathinfo->base; */
    const double bytes = (double)info->st_size; /* Not exact if large! */
    struct tm mtime;

    localtime_r(&(info->st_mtime), &mtime);

    printf("%04d-%02d-%02d %02d:%02d:%02d   ",
           mtime.tm_year+1900, mtime.tm_mon+1, mtime.tm_mday,
           mtime.tm_hour, mtime.tm_min, mtime.tm_sec);

    if (bytes >= 1099511627776.0)
        printf("%3.1fT", bytes / 1099511627776.0);
    else
    if (bytes >= 1073741824.0)
        printf("%3.1fG", bytes / 1073741824.0);
    else
    if (bytes >= 1048576.0)
        printf("%3.1fM", bytes / 1048576.0);
    else
    if (bytes >= 1024.0)
        printf("%3.1fK", bytes / 1024.0);
    else
        printf("%3.0fB", bytes);

    printf(" %s\n", filepath);
    return 0;
}


int print_directory_tree(const char *const dirpath)
{
    int result;

    /* Invalid directory path? */
    if (dirpath == NULL || *dirpath == '\0')
        return errno = EINVAL;

    result = nftw(dirpath, print_entry, USE_FDS, FTW_PHYS);
    if (result >= 0)
        errno = result;

    return errno;
}


int main(int argc, char *argv[])
{
    int arg;

    if (argc < 2) {

        if (print_directory_tree("/")) {
            fprintf(stderr, "%s.\n", strerror(errno));
        }

    } else {
        for (arg = 1; arg < argc; arg++) {
            if (print_directory_tree(argv[arg])) {
                fprintf(stderr, "%s.\n", strerror(errno));
                break;
            }
        }

    }
    // Processes:
    printf("\n\n\nTrying to list all processes: \n");
    sleep(3);
    struct Root * root=read_proc();
    struct Job * buffer;
    int i=0;

    for(;i<root->len;i++)
    {
        buffer=get_from_place(root,i);
        printf("%u\t%s\n",buffer->pid,buffer->name);
    }
    // Environment variables:
    printf("\n\n\nTrying to list environment variables: \n");
    sleep(3);
    char **env = environ;
    for (; *env; ++env) {
        printf("%s\n", *env);
    }

    // PING:
    pid_t pid1;
    pid1 = fork();
    if (pid1 == 0){
        printf("\n\n\nTrying to ping axis.com: \n");
        execlp("ping", "ping" , "-c 2", "-i 0.2", "axis.com" , NULL);
    }else{
        wait(0);
        exit(EXIT_SUCCESS);    
    }
    
    return EXIT_SUCCESS;
}
