git clone https://hagfjall@bitbucket.org/hagfjall/test-program-for-thesis.git testprogram && \
cd testprogram && \
sudo apt-get install -f build-essential cmake make && \
rm -rf build && \
mkdir build && \
cd build && \
cmake .. && \
make && \
mv testprogram .. && \
cd ..
